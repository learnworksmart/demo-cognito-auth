resource "aws_s3_bucket_object" "upload_root_files" {
  for_each      = fileset("src/", "*")
  bucket        = var.project
  key           = each.value
  source        = "src/${each.value}"
  etag          = filemd5("src/${each.value}")
  content_type  = lookup(var.content_type, element(split(".", each.value), length(split(".", each.value))-1), "text/html")
}

resource "aws_s3_bucket_object" "create_js_folder" {
  bucket  = var.project
  acl     = "private"
  key     =  "js/"
  content_type = "application/x-directory"
}

resource "aws_s3_bucket_object" "upload_js_files" {
  for_each      = fileset("src/js/", "*")
  bucket        = var.project
  key           = "js/${each.value}"
  source        = "src/js/${each.value}"
  etag          = filemd5("src/js/${each.value}")
  content_type  = lookup(var.content_type, element(split(".", each.value), length(split(".", each.value))-1), "text/html")
}

variable "content_type" {
  type = map(string)
  description = "The file MIME types"
  default = {
    "html" = "text/html"
    "htm" = "text/html"
    "svg" = "image/svg+xml"
    "jpg" = "image/jpeg"
    "jpeg" = "image/jpeg"
    "gif" = "image/gif"
    "png" = "application/pdf"
    "css" = "text/css"
    "js" = "application/javascript"
    "txt" = "text/plain"
  }
}
