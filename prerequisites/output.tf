output "domain" {
  value = var.project
}

output "region" {
  value = var.region
}

output "appClientId" {
  value = aws_cognito_user_pool_client.client.id
}

output "userPoolId" {
  value = aws_cognito_user_pool_client.client.user_pool_id
}

output "redirectURI" {
  value = "https://${aws_cloudfront_distribution.s3_distribution.domain_name}/index.html"
}
