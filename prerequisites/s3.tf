resource "aws_s3_bucket" "host_web_pages" {
  bucket        = var.project
  acl           = "private"
  force_destroy = true

  versioning {
    enabled = true
  }

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags = {
    Name        = var.project
    Project     = var.project
  }
}

resource "aws_s3_bucket_public_access_block" "host_web_pages" {
  bucket = aws_s3_bucket.host_web_pages.id

  block_public_acls   = false
  block_public_policy = false
  ignore_public_acls  = false
  restrict_public_buckets = false
}

# resource "aws_s3_bucket_policy" "host_web_pages" {
#   bucket = aws_s3_bucket.host_web_pages.id
#   policy = jsonencode({
#     Version = "2012-10-17"
#     Id      = "S3_WEB_HOSTING_POLICY"
#     Statement = [
#       {
#         Sid       = "PublicReadGetObject"
#         Effect    = "Allow"
#         Principal = "*"
#         Action    = "s3:GetObject"
#         Resource = [
#           "${aws_s3_bucket.host_web_pages.arn}/*",
#         ]
#       },
#     ]
#   })
# }

# resource "aws_s3_bucket_object" "index_html" {
#   bucket        = aws_s3_bucket.host_web_pages.bucket
#   acl           = "private"
#   key           = "index.html"
#   source        = "src/index.html"
#   etag          = filemd5("src/index.html")
#   content_type  = "text/html"
# }

# resource "aws_s3_bucket_object" "error_html" {
#   bucket        = aws_s3_bucket.host_web_pages.bucket
#   acl           = "private"
#   key           = "error.html"
#   source        = "src/error.html"
#   etag          = filemd5("src/error.html")
#   content_type  = "text/html"
# }

resource "aws_s3_bucket_object" "upload_root_files" {
  for_each      = fileset("src/", "*")
  bucket        = aws_s3_bucket.host_web_pages.id
  key           = each.value
  source        = "src/${each.value}"
  etag          = filemd5("src/${each.value}")
  content_type  = lookup(var.content_type, element(split(".", each.value), length(split(".", each.value))-1), "text/html")
  # for_each = fileset("src/", "${var.file_path}**")
  # content_type = lookup(var.content_type, element(split(".", each.value), length(split(".", each.value))-1), "text/html")
  # bucket = aws_s3_bucket.host_web_pages.id
  # key = replace(each.value, var.file_path, "")
  # source = each.value
}

# resource "aws_s3_bucket_object" "create_js_folder" {
#   bucket = aws_s3_bucket.host_web_pages.id
#   acl     = "private"
#   key     =  "js/"
#   content_type = "application/x-directory"
# }

# resource "aws_s3_bucket_object" "upload_js_files" {
#   for_each      = fileset("src/js/", "*")
#   bucket        = aws_s3_bucket.host_web_pages.id
#   key           = "js/${each.value}"
#   source        = "src/js/${each.value}"
#   etag          = filemd5("src/js/${each.value}")
#   content_type  = "application/javascript"
# }

variable "content_type" {
  type = map(string)
  description = "The file MIME types"
  default = {
    "html" = "text/html"
    "htm" = "text/html"
    "svg" = "image/svg+xml"
    "jpg" = "image/jpeg"
    "jpeg" = "image/jpeg"
    "gif" = "image/gif"
    "png" = "application/pdf"
    "css" = "text/css"
    "js" = "application/javascript"
    "txt" = "text/plain"
  }
}

variable "file_path" {
  type = string
  description = "The path to the folder of the files you want to upload to S3"
  default = ""
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.host_web_pages.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "host_web_pages" {
  bucket = aws_s3_bucket.host_web_pages.id
  policy = data.aws_iam_policy_document.s3_policy.json
}
