resource "aws_cognito_user_pool" "pool" {
  name                      = var.project
  alias_attributes          = ["email"] #this limit an email can only be used to register for an account.
  auto_verified_attributes  = ["email"]

  mfa_configuration          = "ON"
  sms_authentication_message = "Your code is {####}"

  sms_configuration {
    external_id    = "example"
    sns_caller_arn = aws_iam_role.cognito.arn
  }

  software_token_mfa_configuration {
    enabled = true
  }

  account_recovery_setting {
    recovery_mechanism {
      name     = "verified_email"
      priority = 1
    }
  
    # recovery_mechanism {
    #   name     = "verified_phone_number"
    #   priority = 2
    # }
  }

  password_policy {
    minimum_length    = 8
    require_lowercase = false
    require_numbers   = true
    require_symbols   = true
    require_uppercase = true
    temporary_password_validity_days = 7
  }

  username_configuration {
    case_sensitive = false
  }

  schema {
    name                = "email"
    attribute_data_type = "String"
    mutable             = false
    required            = true

    string_attribute_constraints {
      min_length = 5
      max_length = 2048
    }
  }

  schema {
    name                = "phone_number"
    attribute_data_type = "String"
    mutable             = true
    required            = true

    string_attribute_constraints {
      min_length = 9
      max_length = 15
    }
  }

  tags = {
    Name        = var.project
    Project     = var.project
  }
}

resource "aws_cognito_user_pool_domain" "domain" {
  domain          = var.project
  user_pool_id    = aws_cognito_user_pool.pool.id
}

resource "aws_cognito_user_pool_client" "client" {
  name                                  = var.project
  user_pool_id                          = aws_cognito_user_pool.pool.id
  explicit_auth_flows                   = ["ALLOW_CUSTOM_AUTH", "ALLOW_USER_SRP_AUTH", "ALLOW_REFRESH_TOKEN_AUTH"]
  prevent_user_existence_errors         = "ENABLED" #prevent error message to disclose account existence. 
  callback_urls                         = ["https://${aws_cloudfront_distribution.s3_distribution.domain_name}/index.html"]
  logout_urls                           = ["https://${aws_cloudfront_distribution.s3_distribution.domain_name}/error.html"]
  allowed_oauth_flows                   = ["code"]
  allowed_oauth_scopes                  = ["openid"]
  supported_identity_providers          = ["COGNITO"]
  allowed_oauth_flows_user_pool_client  = true
}

# resource "aws_cognito_user_pool_ui_customization" "pool" {
#   css        = ".label-customizable {font-weight: 300;}"
#   image_file = filebase64("logo.png")
#   # Refer to the aws_cognito_user_pool_domain resource's
#   # user_pool_id attribute to ensure it is in an 'Active' state
#   user_pool_id = aws_cognito_user_pool_domain.domain.user_pool_id
# }
