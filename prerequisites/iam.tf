resource "aws_iam_role_policy_attachment" "cognito" {
  role       = aws_iam_role.cognito.name
  policy_arn = aws_iam_policy.cognito.arn
}

resource "aws_iam_role" "cognito" {
  name               = var.project
  assume_role_policy = data.aws_iam_policy_document.cognito_assume_role.json
  path               = "/"
  description        = "This role is created for Cognito SMS configuration"

  tags      = { 
    Name    = var.project
    Project = var.project
  }
}

data "aws_iam_policy_document" "cognito_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["cognito-idp.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "cognito" {
  name        = var.project
  policy      = data.aws_iam_policy_document.cognito_access_rights.json
  path        = "/"
  description = "This policy is created for ${var.project}."
}

data "aws_iam_policy_document" "cognito_access_rights" {
  statement {
      effect    = "Allow"
      actions   = ["sns:publish"]
      resources = ["*"]     
  }
}
