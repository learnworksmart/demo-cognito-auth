# demo-cognito-auth
This project demonstrates on how to [add authentication to a single-page web application with Amazon Cognito OAuth2 implementation](https://aws.amazon.com/blogs/security/how-to-add-authentication-single-page-web-application-with-amazon-cognito-oauth2-implementation/). In addition, [Cognito MFA](https://docs.aws.amazon.com/cognito/latest/developerguide/user-pool-settings-mfa.html) is enabled for this demonstration. 

## Prerequisites
1. On your local machine, setup the [AWS Credentials File and Credential Profiles](https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials_profiles.html)  with neccessary access rights to your AWS environment. 
1. Clone this project to your local machine.
1. Set a unique string on variable `project`, which can be found under `prerequisites/variables.tf` & `deploy_app/variables.tf`. 
1. Run `cd prerequisites && terraform init && terraform apply --auto-approve`. 
1. Once the run is completed, take note of the outputs which is needed for deploying our web application. 
![image](/uploads/dc7500e053c8f31b8b8a8c1d872475a5/image.png)

## Deploy Web Application
1. Assigned following variables found on `/deploy_app/src/js/userprofile.js`, with the captured values from **Prerequisites** stage.
    * var domain
    * var region
    * var appClientId
    * var userPoolId
    * var redirectURI
1. Run `cd ../deploy_app && terraform init && terraform apply --auto-approve`
1. Once the run has successfully completed, visit the cloudfront URL which can be found in **Prerequisites** stage's `redirectURI`.  
![image](/uploads/5df7160f821d8f25012734b61d63bafd/image.png)
1. The current setup enables **MFA**, where: 
    1. User registration requires a valid email and phone number. 
        ![image](/uploads/de310895ec0ee473da90a6a75a4e6ed6/image.png)
    1. User would need an **OTP** when registering a new account or login to their existing account. 

## References
1. https://aws.amazon.com/blogs/security/how-to-add-authentication-single-page-web-application-with-amazon-cognito-oauth2-implementation/
1. https://aws.amazon.com/getting-started/hands-on/build-serverless-web-app-lambda-apigateway-s3-dynamodb-cognito/module-3/
1. https://medium.com/@eelkevdbos/how-to-managing-user-registration-and-login-using-aws-cognito-9d5b51034d13
